package com.moujib.backend.model.product;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Clothe implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String clotheType;

    private String sex;

    private String size;

    public int getId() {
        return id;
    }

    public String getClotheType() {
        return clotheType;
    }

    public String getSex() {
        return sex;
    }

    public String getSize() {
        return size;
    }
}
