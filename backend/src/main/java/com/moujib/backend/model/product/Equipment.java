package com.moujib.backend.model.product;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class Equipment extends Product implements Serializable {

    private String equipmentType;

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }
}

